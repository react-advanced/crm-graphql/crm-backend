import { ApolloServer } from "apollo-server";
import connectDB from "./config/database";
import CRMSchemaBuilder from "./graphql/CRMSchemaBuilder";
import CRMContextBuilder from "./graphql/CRMContextBuilder";
import { config as envConfig } from "dotenv";

envConfig({ path: "properties.env" });

const bootstrap = async () => {
	await connectDB();

	const schemaBuilder = new CRMSchemaBuilder();
	const conextBuilder = new CRMContextBuilder();

	const schema = await schemaBuilder.build();
	const context = conextBuilder.build();

	const server = new ApolloServer({
		schema,
		context,
		cors: {
			origin: process.env.CORS_ORIGIN,
			methods: process.env.CORS_METHODS,
			allowedHeaders: ["Content-Type", "Authorization"],
			preflightContinue: false,
			optionsSuccessStatus: 204,
			credentials: true,
		},
	});

	const { url } = await server.listen(process.env.PORT || 4000);

	console.log(`Server running at ${url}`, "🚀🚀🚀");
};

bootstrap();
