import Product from "../model/Product";
import { IProduct } from "../types/IProduct";

class ProductService {
	async save({ name, stock, price }): Promise<IProduct> {
		try {
			const product = new Product({ name, stock, price });
			await product.save();
			return product;
		} catch (e) {
			const message = "Error creating product 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async update(id, { name, stock, price }): Promise<IProduct> {
		try {
			const product = await Product.findOneAndUpdate({ _id: id }, { name, stock, price }, { new: true }).exec();
			return product;
		} catch (e) {
			const message = "Error updating product 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async deleteById(id): Promise<IProduct> {
		try {
			const product = await Product.findOneAndDelete({ _id: id }).exec();
			return product;
		} catch (e) {
			const message = "Error deleting user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findAll(): Promise<IProduct[]> {
		try {
			const products = await Product.find().exec();
			return products;
		} catch (e) {
			const message = "Error searching for products 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findById(id): Promise<IProduct> {
		try {
			const product = await Product.findOne({ _id: id }).exec();
			if (!product) throw new Error();
			return product;
		} catch (e) {
			const message = "The requested product was not found 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findBySimilarName(name): Promise<IProduct[]> {
		try {
			const products = await Product.find({ $text: { $search: name } }).exec();
			return products;
		} catch (e) {
			const message = "Error searching for products 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}
}

export default ProductService;
