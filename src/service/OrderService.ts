import OrderStatus from "../constant/OrderStatus";
import Order from "../model/Order";
import { IOrder } from "../types/IOrder";
import ClientService from "./ClientService";
import ProductService from "./ProductService";

class OrderService {
	private clientService;
	private productService;

	constructor({ clientService = new ClientService(), productService = new ProductService() } = {}) {
		this.clientService = clientService;
		this.productService = productService;
	}

	async save({ productOrders, total, client, status, user }): Promise<IOrder> {
		try {
			const exist = this.clientService.findByIdAndSeller(client, user);
			if (!exist) {
				throw new Error(`The client ${client} does not exist or the user is invalid`);
			}

			const order = new Order({ productOrders, total, client, status, user });

			switch (status) {
				case OrderStatus.PENDING:
				case OrderStatus.CANCELED: {
					for await (const { id, quantity } of productOrders) {
						const product = await this.productService.findById(id);
						if (!product) {
							throw new Error(`Product with id ${id} does not exist`);
						}

						if (quantity > product.stock) {
							throw new Error(`There is not enough stock of the product requested; currently ${product.stock}`);
						}
					}
					break;
				}
				case OrderStatus.COMPLETED: {
					await this.updateProductOrders(productOrders);
					break;
				}
				default: {
					throw new Error("Invalid order status");
				}
			}

			await order.save();
			return order;
		} catch (e) {
			const message = `Error creating order 😲😲😲: ${e.message}`;
			console.log(message);
			throw new Error(message);
		}
	}

	async update(id, { productOrders, total, client, status, user }): Promise<IOrder> {
		try {
			let order = await this.findByIdAndUser(id, user);
			if (!order) {
				throw new Error("Order does not exist");
			}

			if (order.status !== OrderStatus.PENDING) {
				throw new Error("You cannot update an order that is not pending");
			}

			const clientData = await this.clientService.findById(client);
			if (!clientData) {
				throw new Error("Client does not exist");
			}

			switch (status) {
				case OrderStatus.PENDING:
				case OrderStatus.CANCELED: {
					order = await Order.findOneAndUpdate({ _id: id, user }, { productOrders, total, client, status, user }, { new: true }).exec();
					break;
				}
				case OrderStatus.COMPLETED: {
					await this.updateProductOrders(productOrders);
					order = await Order.findOneAndUpdate({ _id: id, user }, { productOrders, total, client, status, user }, { new: true }).exec();
					break;
				}
				default: {
					throw new Error("Invalid order status");
				}
			}

			return order;
		} catch (e) {
			const message = "Error updating order 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async deleteById(id, user): Promise<IOrder> {
		try {
			let order = await this.findByIdAndUser(id, user);
			if (!order) {
				throw new Error("Order does not exist");
			}

			if (order.status !== OrderStatus.PENDING) {
				throw new Error("You cannot delete an order that is not pending");
			}

			await Order.deleteOne({ _id: id }).exec();

			return order;
		} catch (e) {
			const message = "Error deleting order 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	private async updateProductOrders(productOrders) {
		const productsToUpdate = [];

		for await (const { id: productId, quantity } of productOrders) {
			const product = await this.productService.findById(productId);
			if (!product) {
				throw new Error(`Product with id ${productId} does not exist`);
			}

			if (quantity > product.stock) {
				throw new Error(`There is not enough stock of the product requested; currently ${product.stock}`);
			}

			product.stock -= quantity;
			productsToUpdate.push(product);
		}

		for await (const product of productsToUpdate) {
			await this.productService.update(product.id, product);
		}
	}

	async findAll(): Promise<IOrder[]> {
		try {
			const order = await Order.find().populate("client").exec();
			return order;
		} catch (e) {
			const message = "Error searching for orders 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByUser(userId): Promise<IOrder[]> {
		try {
			const orders = await Order.find({ user: userId }).populate("client").exec();
			return orders;
		} catch (e) {
			const message = "Error searching for orders 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByStatus(status, user): Promise<IOrder[]> {
		try {
			const orders = await Order.find({ status, user }).populate("client").exec();
			return orders;
		} catch (e) {
			const message = "Error searching for orders 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findById(id): Promise<IOrder> {
		try {
			const order = await Order.findOne({ _id: id }).populate("client").exec();
			if (!order) throw new Error();
			return order;
		} catch (e) {
			const message = "The requested order was not found 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByIdAndUser(id, userId): Promise<IOrder> {
		try {
			const order = await Order.findOne({ _id: id, user: userId }).populate("client").exec();
			if (!order) throw new Error();
			return order;
		} catch (e) {
			const message = "The requested order was not found 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}
}

export default OrderService;
