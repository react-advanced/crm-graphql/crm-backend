import Order from "../model/Order";
import OrderStatus from "../constant/OrderStatus";

class StatisticsService {
	async findTopClients() {
		const topClients = await Order.aggregate([
			{ $match: { status: OrderStatus.COMPLETED } },
			{
				$group: {
					_id: "$client",
					total: { $sum: "$total" },
				},
			},
			{
				$lookup: {
					from: "clients",
					localField: "_id",
					foreignField: "_id",
					as: "client",
				},
			},
			{
				$sort: { total: -1 },
			},
		]).exec();
		return topClients;
	}

	async findTopSellers() {
		const topSellers = await Order.aggregate([
			{ $match: { status: OrderStatus.COMPLETED } },
			{
				$group: {
					_id: "$user",
					total: { $sum: "$total" },
				},
			},
			{
				$lookup: {
					from: "users",
					localField: "_id",
					foreignField: "_id",
					as: "seller",
				},
			},
			{
				$sort: { total: -1 },
			},
		]).exec();
		return topSellers;
	}
}

export default StatisticsService;
