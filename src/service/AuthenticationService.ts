import { config as envConfig } from "dotenv";
import jwt from "jsonwebtoken";
import UserStatus from "../constant/UserStatus";
import { IAuthentication } from "../types/IAuthentication";
import { IUser } from "../types/IUser";
import UserService from "./UserService";

envConfig({ path: "properties.env" });

class AuthenticationService {
	static AUTHENTICATION_ERROR_MESSAGE = "The user entered does not exist or the data entered is incorrect.";
	static INACTIVE_USER_ERROR_MESSAGE = "The user is inactive. Contact an administrator.";
	private userService;

	constructor({ userService = new UserService() } = {}) {
		this.userService = userService;
	}

	private getToken(user: IUser, secretWord: string, secondsToExpire: number): IAuthentication {
		const { id } = user;
		const payload = { id };
		//console.log("PAYLOAD: ", payload);
		//console.log("TOKEN: ", token);
		const createAt = Date.now();
		const expiresAt = createAt + secondsToExpire * 1000;
		const token = jwt.sign(payload, secretWord, { expiresIn: secondsToExpire });
		return { accessToken: token, createAt: createAt.toString(), expiresAt: expiresAt.toString() };
	}

	validate(token: string): boolean {
		try {
			const valid = jwt.verify(token, process.env.SECRET_WORD);
			return !!valid;
		} catch (e) {
			//console.error(e);
			return false;
		}
	}

	async getUser(token: string): Promise<IUser> {
		if (!this.validate(token)) {
			return null;
		}
		const { id } = jwt.decode(token, process.env.SECRET_WORD);
		const user = await this.userService.findById(id);
		return user;
	}

	async authenticate({ email, password }): Promise<IAuthentication> {
		const user = await this.userService.findByEmail(email);

		if (!user) {
			console.log(AuthenticationService.AUTHENTICATION_ERROR_MESSAGE);
			throw new Error(AuthenticationService.AUTHENTICATION_ERROR_MESSAGE);
		}

		const validPassword = await this.userService.compareAgainstEncryptedPassword(password, user.password);
		if (!validPassword) {
			console.log(AuthenticationService.AUTHENTICATION_ERROR_MESSAGE);
			throw new Error(AuthenticationService.AUTHENTICATION_ERROR_MESSAGE);
		}

		if (user.status === UserStatus.INACTIVE) {
			console.log(AuthenticationService.INACTIVE_USER_ERROR_MESSAGE);
			throw new Error(AuthenticationService.INACTIVE_USER_ERROR_MESSAGE);
		}

		return this.getToken(user, process.env.SECRET_WORD, 1 * 60 * 60 * 0.5);
	}
}

export default AuthenticationService;
