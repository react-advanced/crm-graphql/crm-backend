import bcrypt from "bcrypt";
import User from "../model/User";
import { IUser } from "../types/IUser";
import UserRole from "./../constant/UserRole";
import UserStatus from "./../constant/UserStatus";

class UserService {
	async encrypt(text): Promise<string> {
		const salt = await bcrypt.genSalt(10);
		const encryptedText = await bcrypt.hash(text, salt);
		return encryptedText;
	}

	async compareAgainstEncryptedPassword(plaintextPassword, password): Promise<boolean> {
		const equals = await bcrypt.compare(plaintextPassword, password);
		return equals;
	}

	async exist(email): Promise<boolean> {
		const user = await User.findOne({ email }).exec();
		return !!user;
	}

	async save({ firstname, lastname, email, password: plaintextPassword }): Promise<IUser> {
		const exist = await this.exist(email);
		if (exist) {
			const message = `Error, user with email ${email} already exists`;
			console.log(message);
			throw new Error(message);
		}

		try {
			const password = await this.encrypt(plaintextPassword);
			const user = new User({ firstname, lastname, email, password, status: UserStatus.INACTIVE, role: UserRole.USER });
			await user.save();
			return user;
		} catch (e) {
			const message = "Error creating user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async update(userId, userUpdateId, { id, firstname, lastname, email, role, status }): Promise<IUser> {
		const user = await this.findById(userId);
		if (user.role !== UserRole.ADMIN) {
			const message = "The user you are trying to update with is not an administrator.";
			console.log(message);
			throw new Error(message);
		}

		try {
			return await User.findOneAndUpdate({ _id: userUpdateId }, { firstname, lastname, email, role, status }, { new: true }).exec();
		} catch (e) {
			const message = "Error updating user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findAll(): Promise<IUser[]> {
		try {
			const users = await User.find().exec();
			return users;
		} catch (e) {
			const message = "Error searching for users 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByEmail(email): Promise<IUser> {
		try {
			const user = await User.findOne({ email }).exec();
			return user;
		} catch (e) {
			const message = "Error finding user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findById(id): Promise<IUser> {
		try {
			const user = await User.findOne({ _id: id }).exec();
			if (!user) throw new Error();
			return user;
		} catch (e) {
			const message = "Error finding user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async deleteById(id): Promise<IUser> {
		try {
			const user = await User.findOneAndRemove({ _id: id }).exec();
			if (!user) throw new Error();
			return user;
		} catch (e) {
			const message = "Error deleting user 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}
}

export default UserService;
