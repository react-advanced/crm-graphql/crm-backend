import Client from "../model/Client";
import { IClient } from "../types/IClient";

class ClientService {
	async exist(email): Promise<boolean> {
		const client = await Client.findOne({ email }).exec();
		return !!client;
	}

	async save({ firstname, lastname, email, phone, company, seller }): Promise<IClient> {
		const exist = await this.exist(email);
		if (exist) {
			const message = `Error, client with email ${email} already exists`;
			console.log(message);
			throw new Error(message);
		}

		try {
			const client = new Client({ firstname, lastname, email, phone, company, seller });
			await client.save();
			return client;
		} catch (e) {
			const message = "Error creating client 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async update(id, sellerId, { firstname, lastname, email, phone, company }): Promise<IClient> {
		try {
			const client = await Client.findOneAndUpdate(
				{ _id: id, seller: sellerId },
				{ firstname, lastname, email, phone, company },
				{ new: true }
			).exec();
			return client;
		} catch (e) {
			const message = "Error updating client 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findAll(): Promise<IClient[]> {
		try {
			const clients = await Client.find().exec();
			return clients;
		} catch (e) {
			const message = "Error searching for clients 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findById(id): Promise<IClient> {
		try {
			const client = await Client.findOne({ _id: id }).exec();
			if (!client) throw new Error();
			return client;
		} catch (e) {
			const message = "The requested client was not found 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByEmail(email): Promise<IClient> {
		try {
			const client = await Client.findOne({ email }).exec();
			if (!client) throw new Error();
			return client;
		} catch (e) {
			const message = "The requested client was not found 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findBySeller(id): Promise<IClient[]> {
		try {
			const clients = await Client.find({ seller: id }).exec();
			return clients;
		} catch (e) {
			const message = "Error searching for clients 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async findByIdAndSeller(id, sellerId): Promise<IClient> {
		try {
			const clients = await Client.findOne({ _id: id, seller: sellerId }).exec();
			return clients;
		} catch (e) {
			const message = "Error searching for clients 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async delete(id): Promise<IClient> {
		try {
			const client = await Client.findOneAndDelete({ _id: id }).exec();
			if (!client) throw new Error();
			return client;
		} catch (e) {
			const message = "Error deleting client 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}

	async deleteBySeller(id, seller): Promise<IClient> {
		try {
			const client = await Client.findOneAndDelete({ _id: id, seller }).exec();
			if (!client) throw new Error();
			return client;
		} catch (e) {
			const message = "Error deleting client 😲😲😲";
			console.log(message, e);
			throw new Error(message);
		}
	}
}

export default ClientService;
