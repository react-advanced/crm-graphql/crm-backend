import mongoose from "mongoose";
import { config as envConfig } from "dotenv";

envConfig({ path: "properties.env" });

const connectDB = async () => {
	try {
		await mongoose.connect(process.env.DB_URL, {
			user: process.env.DB_USER,
			pass: process.env.DB_PASSWORD,
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		});
		console.log("Successful database connection 😄😄😄");
	} catch (e) {
		console.log("Error connecting to database 😲😲😲", e);
		process.exit(1);
	}
};

export default connectDB;
