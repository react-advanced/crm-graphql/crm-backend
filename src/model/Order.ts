import { model, Schema, Types } from "mongoose";
import { IOrder } from "../types/IOrder";

const OrderSchema = new Schema({
	productOrders: {
		type: Array,
		required: true,
	},
	total: {
		type: Number,
		required: true,
	},
	client: {
		type: Types.ObjectId,
		required: true,
		ref: "Client",
	},
	user: {
		type: Types.ObjectId,
		required: true,
		ref: "User",
	},
	status: {
		type: String,
		default: "PENDING",
	},
	createAt: {
		type: Date,
		default: Date.now(),
	},
});

export default model<IOrder>("Order", OrderSchema);
