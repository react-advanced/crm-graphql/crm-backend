import { model, Schema } from "mongoose";
import { IProduct } from "../types/IProduct";

const ProductSchema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true,
	},
	stock: {
		type: Number,
		required: true,
		trim: true,
	},
	price: {
		type: Number,
		required: true,
		trim: true,
	},
	createAt: {
		type: Date,
		default: Date.now(),
	},
});

ProductSchema.index({ name: "text" });

export default model<IProduct>("Product", ProductSchema);
