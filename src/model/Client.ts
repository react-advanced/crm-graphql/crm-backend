import { model, Schema, Types } from "mongoose";
import { IClient } from "../types/IClient";

const stringField = {
	type: String,
	required: true,
	trim: true,
};

const ClientSchema = new Schema({
	firstname: { ...stringField },
	lastname: { ...stringField },
	email: {
		...stringField,
		unique: true,
	},
	phone: {
		...stringField,
		required: false,
	},
	company: { ...stringField },
	createAt: {
		type: Date,
		default: Date.now(),
	},
	seller: {
		type: Types.ObjectId,
		required: true,
		ref: "User",
	},
});

export default model<IClient>("Client", ClientSchema);
