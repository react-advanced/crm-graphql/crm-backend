import { model, Schema } from "mongoose";
import { IUser } from "../types/IUser";

const stringField = {
	type: String,
	required: true,
	trim: true,
};

const UserSchema = new Schema({
	firstname: { ...stringField },
	lastname: { ...stringField },
	email: {
		...stringField,
		unique: true,
	},
	password: { ...stringField },
	role: { ...stringField },
	status: { ...stringField },
	createAt: {
		type: Date,
		default: Date.now(),
	},
});

export default model<IUser>("User", UserSchema);
