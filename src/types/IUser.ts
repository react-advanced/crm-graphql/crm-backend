import { Document, Types } from "mongoose";

export enum UserRole {
	ADMIN = "ADMIN",
	USER = "USER",
}

export enum UserStatus {
	ACTIVE = "ACTIVE",
	INACTIVE = "INACTIVE",
}

export interface IUser extends Document {
	id: Types.ObjectId;
	firstname: string;
	lastname: string;
	password: string;
	email: string;
	role: UserRole;
	status: UserStatus;
	createAt: Date;
}
