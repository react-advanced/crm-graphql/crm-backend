import { Document, Types } from "mongoose";

export enum OrderStatus {
	PENDING = "PENDING",
	COMPLETED = "COMPLETED",
	CANCELED = "CANCELED",
}

export interface IProductOrder {
	id: Types.ObjectId;
	quantity: number;
}

export interface IOrder extends Document {
	id: Types.ObjectId;
	productOrders: [IProductOrder];
	total: number;
	client: Types.ObjectId;
	user: Types.ObjectId;
	status: OrderStatus;
	createAt: string;
}
