import { Document, Types } from "mongoose";

export interface IProduct extends Document {
	id: Types.ObjectId;
	name: string;
	stock: number;
	price: number;
	createAt: string;
}
