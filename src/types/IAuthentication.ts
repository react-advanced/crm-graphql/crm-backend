export interface IAuthentication {
	accessToken: string;
	createAt: string;
	expiresAt: string;
}
