import { Document, Types } from "mongoose";

export interface IClient extends Document {
	id: Types.ObjectId;
	firstname: string;
	lastname: string;
	email: string;
	phone: string;
	company: string;
	createAt: Date;
	seller: Types.ObjectId;
}
