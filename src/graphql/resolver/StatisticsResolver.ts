import StatisticsService from "../../service/StatisticsService";
import TokenValidator from "../middleware/TokenValidator";
import ResolverBuilder from "../ResolverBuilder";

class StatisticsResolver {
	private statisticsService: StatisticsService;
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;

	constructor({ statisticsService = new StatisticsService(), resolverBuilder = new ResolverBuilder(), tokenValidator = new TokenValidator() } = {}) {
		this.statisticsService = statisticsService;
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
	}

	build() {
		const middleware = this.tokenValidator.getMiddleware();

		this.resolverBuilder.withMiddleware(middleware).addQuery("getTopClients", () => this.statisticsService.findTopClients());
		this.resolverBuilder.withMiddleware(middleware).addQuery("getTopSellers", () => this.statisticsService.findTopSellers());

		return this.resolverBuilder.build();
	}
}

export default StatisticsResolver;
