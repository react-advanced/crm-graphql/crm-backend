import UserService from "../../service/UserService";
import { UserRole } from "../../types/IUser";
import RoleValidator from "../middleware/RoleValidator";
import TokenValidator from "../middleware/TokenValidator";
import ResolverBuilder from "../ResolverBuilder";

class UserResolver {
	private userService: UserService;
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;
	private roleValidator: RoleValidator;

	constructor({
		userService = new UserService(),
		resolverBuilder = new ResolverBuilder(),
		tokenValidator = new TokenValidator(),
		roleValidator = new RoleValidator(),
	} = {}) {
		this.userService = userService;
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
		this.roleValidator = roleValidator;
	}

	build() {
		const middleware = this.tokenValidator.getMiddleware();
		const adminRoleMiddleware = this.roleValidator.getMiddleware(UserRole.ADMIN);

		this.resolverBuilder
			.withMiddleware(middleware)
			.withMiddleware(adminRoleMiddleware)
			.addQuery("getUserById", (_, { id }) => this.userService.findById(id));

		this.resolverBuilder.withMiddleware(middleware).addQuery("getUser", (_, params, { user: { id } }) => this.userService.findById(id));
		this.resolverBuilder.withMiddleware(middleware).addQuery("getUsers", () => this.userService.findAll());
		this.resolverBuilder.addMutation("saveUser", (_, { input }) => this.userService.save(input));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("updateUser", (_, { id, input }, { user }) => this.userService.update(user.id, id, input));

		this.resolverBuilder
			.withMiddleware(middleware)
			.withMiddleware(adminRoleMiddleware)
			.addMutation("deleteUser", (_, { id }) => this.userService.deleteById(id));

		return this.resolverBuilder.build();
	}
}

export default UserResolver;
