import ClientService from "../../service/ClientService";
import TokenValidator from "../middleware/TokenValidator";
import ResolverBuilder from "../ResolverBuilder";

class ClientResolver {
	private clientService: ClientService;
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;

	constructor({ clientService = new ClientService(), resolverBuilder = new ResolverBuilder(), tokenValidator = new TokenValidator() } = {}) {
		this.clientService = clientService;
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
	}

	build() {
		const middleware = this.tokenValidator.getMiddleware();

		this.resolverBuilder.withMiddleware(middleware).addQuery("getClients", (_, args, { user: { id } }) => this.clientService.findBySeller(id));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addQuery("getClient", (_, { id }, { user: { id: seller } }) => this.clientService.findByIdAndSeller(id, seller));

		this.resolverBuilder.withMiddleware(middleware).addQuery("getAllClients", () => this.clientService.findAll());

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("saveClient", (_, { input }, { user: { id } }) => this.clientService.save({ ...input, seller: id }));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("updateClient", (_, { id, input }, { user: { id: seller } }) => this.clientService.update(id, seller, input));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("deleteClient", (_, { id }, { user: { id: seller } }) => this.clientService.deleteBySeller(id, seller));

		return this.resolverBuilder.build();
	}
}

export default ClientResolver;
