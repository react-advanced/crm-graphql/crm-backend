import ProductService from "../../service/ProductService";
import TokenValidator from "../middleware/TokenValidator";
import ResolverBuilder from "../ResolverBuilder";

class ProductResolver {
	private productService: ProductService;
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;

	constructor({ productService = new ProductService(), resolverBuilder = new ResolverBuilder(), tokenValidator = new TokenValidator() } = {}) {
		this.productService = productService;
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
	}

	build() {
		const middleware = this.tokenValidator.getMiddleware();

		this.resolverBuilder.withMiddleware(middleware).addQuery("getProducts", () => this.productService.findAll());
		this.resolverBuilder.withMiddleware(middleware).addQuery("getProduct", (_, { id }) => this.productService.findById(id));
		this.resolverBuilder.withMiddleware(middleware).addQuery("getProductsByName", (_, { name }) => this.productService.findBySimilarName(name));
		this.resolverBuilder.withMiddleware(middleware).addMutation("saveProduct", (_, { input }) => this.productService.save(input));
		this.resolverBuilder.withMiddleware(middleware).addMutation("updateProduct", (_, { id, input }) => this.productService.update(id, input));
		this.resolverBuilder.withMiddleware(middleware).addMutation("deleteProduct", (_, { id }) => this.productService.deleteById(id));

		return this.resolverBuilder.build();
	}
}

export default ProductResolver;
