import OrderService from "../../service/OrderService";
import TokenValidator from "../middleware/TokenValidator";
import ResolverBuilder from "../ResolverBuilder";

class OrderResolver {
	private orderService: OrderService;
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;

	constructor({ orderService = new OrderService(), resolverBuilder = new ResolverBuilder(), tokenValidator = new TokenValidator() } = {}) {
		this.orderService = orderService;
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
	}

	build() {
		const middleware = this.tokenValidator.getMiddleware();

		this.resolverBuilder.withMiddleware(middleware).addQuery("getOrders", () => this.orderService.findAll());

		this.resolverBuilder
			.withMiddleware(middleware)
			.addQuery("getOrder", (_, { id }, { user: { id: userId } }) => this.orderService.findByIdAndUser(id, userId));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addQuery("getOrdersByUser", (_, params, { user: { id: userId } }) => this.orderService.findByUser(userId));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addQuery("getOrdersByStatus", (_, { status }, { user: { id: userId } }) => this.orderService.findByStatus(status, userId));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("saveOrder", (_, { input }, { user: { id: userId } }) => this.orderService.save({ ...input, user: userId }));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("updateOrder", (_, { id, input }, { user: { id: userId } }) => this.orderService.update(id, { ...input, user: userId }));

		this.resolverBuilder
			.withMiddleware(middleware)
			.addMutation("deleteOrder", (_, { id }, { user: { id: userId } }) => this.orderService.deleteById(id, userId));

		return this.resolverBuilder.build();
	}
}

export default OrderResolver;
