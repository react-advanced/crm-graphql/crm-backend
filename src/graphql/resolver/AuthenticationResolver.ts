import AuthenticationService from "../../service/AuthenticationService";
import ResolverBuilder from "../ResolverBuilder";

class AuthenticationResolver {
	private authenticationService: AuthenticationService;
	private resolverBuilder: ResolverBuilder;

	constructor({ authenticationService = new AuthenticationService(), resolverBuilder = new ResolverBuilder() } = {}) {
		this.authenticationService = authenticationService;
		this.resolverBuilder = resolverBuilder;
	}

	build() {
		this.resolverBuilder.addMutation("authenticate", (_, { input }) => this.authenticationService.authenticate(input));
		return this.resolverBuilder.build();
	}
}

export default AuthenticationResolver;
