import { join, resolve } from "path";
import { addResolversToSchema, GraphQLFileLoader, loadSchema } from "graphql-tools";
import CRMResolverBuilder from "./CRMResolverBuilder";

class CRMSchemaBuilder {
	static GRAPHQL_SCHEMA_PATH = join(resolve(), "src/graphql/schema/*.graphql");
	private resolverBuilder: CRMResolverBuilder;

	constructor({ resolverBuilder = new CRMResolverBuilder() } = {}) {
		this.resolverBuilder = resolverBuilder;
	}

	async build() {
		const options = { loaders: [new GraphQLFileLoader()] };
		const schema = await loadSchema(CRMSchemaBuilder.GRAPHQL_SCHEMA_PATH, options);
		const schemaWithResolvers = addResolversToSchema(schema, this.resolverBuilder.build());
		return schemaWithResolvers;
	}
}

export default CRMSchemaBuilder;
