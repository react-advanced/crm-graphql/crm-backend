class ResolverBuilder {
	static INVALID_RESOLVER_ERROR_MESSAGE = "The entered resolver is invalid";
	private mutations;
	private queries;

	constructor() {
		this.mutations = {};
		this.queries = {};
	}

	private isValidResolver(key, handler) {
		return !!key && typeof key === "string" && key.length > 0 && !!handler && typeof handler === "function";
	}

	private toResolver(handler) {
		return (...args) => handler(...args);
	}

	withMiddleware(middleware) {
		return {
			addMutation: (key, mutation) => this.addMutation(key, (...args) => middleware(() => mutation(...args), ...args)),
			addQuery: (key, query) => this.addQuery(key, (...args) => middleware(() => query(...args), ...args)),
			withMiddleware: (otherMiddleware) => this.withMiddleware((next, ...args) => middleware(() => otherMiddleware(next, ...args), ...args)),
		};
	}

	addMutation(key, mutation) {
		if (!this.isValidResolver(key, mutation)) {
			console.log(ResolverBuilder.INVALID_RESOLVER_ERROR_MESSAGE);
			throw new Error(ResolverBuilder.INVALID_RESOLVER_ERROR_MESSAGE);
		}
		const newMutations = { ...this.mutations };
		newMutations[key] = this.toResolver(mutation);
		this.mutations = newMutations;
	}

	addQuery(key, query) {
		if (!this.isValidResolver(key, query)) {
			console.log(ResolverBuilder.INVALID_RESOLVER_ERROR_MESSAGE);
			throw new Error(ResolverBuilder.INVALID_RESOLVER_ERROR_MESSAGE);
		}
		const newQueries = { ...this.queries };
		newQueries[key] = this.toResolver(query);
		this.queries = newQueries;
	}

	build() {
		return {
			Query: { ...this.queries },
			Mutation: { ...this.mutations },
		};
	}
}

export default ResolverBuilder;
