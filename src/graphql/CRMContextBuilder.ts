import AuthenticationService from "../service/AuthenticationService";

class CRMContextBuilder {
	private authenticationService: AuthenticationService;

	constructor({ authenticationService = new AuthenticationService() } = {}) {
		this.authenticationService = authenticationService;
	}

	build() {
		return async ({ req }) => ({
			token: req.headers.authorization,
			user: req.headers.authorization && (await this.authenticationService.getUser(req.headers.authorization)),
		});
	}
}

export default CRMContextBuilder;
