import AuthenticationService from "../../service/AuthenticationService";
import { UserRole } from "../../types/IUser";

class RoleValidator {
	private authenticationService: AuthenticationService;

	constructor({ authenticationService = new AuthenticationService() } = {}) {
		this.authenticationService = authenticationService;
	}

	getMiddleware(role: UserRole) {
		return (next, parent, args, { user }) => {
			if (user?.role !== role) {
				const message = "Invalid role";
				throw new Error(message);
			} else {
				return next();
			}
		};
	}
}

export default RoleValidator;
