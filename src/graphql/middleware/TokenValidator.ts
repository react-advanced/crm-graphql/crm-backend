import AuthenticationService from "../../service/AuthenticationService";

class TokenValidator {
	private authenticationService: AuthenticationService;

	constructor({ authenticationService = new AuthenticationService() } = {}) {
		this.authenticationService = authenticationService;
	}

	getMiddleware() {
		return (next, parent, args, { token }) => {
			if (!this.authenticationService.validate(token)) {
				const message = "Invalid token";
				throw new Error(message);
			} else {
				return next();
			}
		};
	}
}

export default TokenValidator;
