import AuthenticationResolver from "./resolver/AuthenticationResolver";
import ClientResolver from "./resolver/ClientResolver";
import OrderResolver from "./resolver/OrderResolver";
import ProductResolver from "./resolver/ProductResolver";
import UserResolver from "./resolver/UserResolver";
import ResolverBuilder from "./ResolverBuilder";
import TokenValidator from "./middleware/TokenValidator";
import { mergeResolvers } from "graphql-tools";
import StatisticsResolver from "./resolver/StatisticsResolver";

class CRMResolverBuilder {
	private resolverBuilder: ResolverBuilder;
	private tokenValidator: TokenValidator;

	constructor({ resolverBuilder = new ResolverBuilder(), tokenValidator = new TokenValidator() } = {}) {
		this.resolverBuilder = resolverBuilder;
		this.tokenValidator = tokenValidator;
	}

	build() {
		const args = { resolverBuilder: this.resolverBuilder, tokenValidator: this.tokenValidator };

		const authenticationResolver = new AuthenticationResolver(args);
		const clientResolver = new ClientResolver(args);
		const orderResolver = new OrderResolver(args);
		const productResolver = new ProductResolver(args);
		const statisticsResolver = new StatisticsResolver(args);
		const userResolver = new UserResolver(args);

		return mergeResolvers([
			authenticationResolver.build(),
			clientResolver.build(),
			orderResolver.build(),
			productResolver.build(),
			statisticsResolver.build(),
			userResolver.build(),
		]);
	}
}

export default CRMResolverBuilder;
