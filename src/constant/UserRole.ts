const UserRole = Object.freeze({
	ADMIN: "ADMIN",
	USER: "USER",
});

export default UserRole;
