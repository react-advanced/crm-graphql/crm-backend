const OrderStatus = Object.freeze({
	PENDING: "PENDING",
	COMPLETED: "COMPLETED",
	CANCELED: "CANCELED",
});

export default OrderStatus;
