const UserStatus = Object.freeze({
	ACTIVE: "ACTIVE",
	INACTIVE: "INACTIVE",
});

export default UserStatus;
